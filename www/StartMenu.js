SpaceCollector.StartMenu = function (game) {
    this.startBG;
    this.startPrompt;
}

SpaceCollector.StartMenu.prototype = {

    create: function () {
        console.log("In startMenu create()");
        // bind event handler to the screen, when the screen is touched this.startGame is called
        this.game.input.onDown.add(this.startGame, this);
        this.startPrompt = this.add.bitmapText(this.world.centerX - 155, this.world.centerY + 380, 'eightbitwonder', 'Touch to Start!', 36);

        // add a tween to the starting prompt to fade it in and out, which helps user see the message
        this.add.tween(this.startPrompt).to({
            alpha: 0
        }, 2200, Phaser.Easing.Linear.None, true, 0, 8000, true);

        // add 12 stars
        for (var i = 0; i < 12; i += 1) {
            // add the star at a random position in the top half of the screen
            var star = this.game.add.sprite(this.rnd.integerInRange(10, 600), this.rnd.integerInRange(20, 280), 'star');
            star.anchor.setTo(0.5, 0.5);
            // scale the stars differently, so they aren;t all the same size
            star.scale.setTo(1.4 * (i * 0.1));
            // all stars have min alpha of 0.2
            star.alpha = 0.2;
            // add a tween to go from fully alpha (1) back to min (0.2) and add make each last a random time
            this.add.tween(star).to({
                alpha: 1
            }, this.rnd.integerInRange(1100, 4200), Phaser.Easing.Linear.None, true, 0, 8000, true);
        }

        // add the start image after the star animations, so that the stars don't sit on top of image
        this.add.image(0, 0, 'startGameScreen');
    },
    // event handler function, called when screen is clicked
    startGame: function (pointer) {
        console.log("In startGame -------");
        // start the main game state
        this.state.start('Game');
    }
};