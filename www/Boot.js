var SpaceCollector = {};

SpaceCollector.Boot = function (game) {};

SpaceCollector.Boot.prototype = {
    preload: function () {
        console.log("Boot preload..");
        // load the loading bar and game title image
        this.load.image('preloaderBar', 'images/loader_bar.png');
        this.load.image('titleimage', 'images/TitleImage.png');
    },

    create: function () {
        console.log("Boot create..");
        // this makes no difference, thought setting pointer to 1 would help with emulators double touch event
        //this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = false;
        // set phasers scale mode to show everything
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // center game within screen
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        // for portrait mode
        this.stage.forcePortrait = true;

        // set the background to black
        this.stage.backgroundColor = '#000000';

        // start the preloader state
        this.state.start('Preloader');
    }
}