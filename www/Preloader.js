SpaceCollector.Preloader = function (game) {
    this.preloadBar = null;
    this.titleText = null;
    this.ready = false;
};

SpaceCollector.Preloader.prototype = {

    preload: function () {
        console.log("Preloader preload...");
        // Display items loaded in the Boot state while loading all image assets
        // display loading bar and set it to the center of the screen
        this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'preloaderBar');
        this.preloadBar.anchor.setTo(0.5, 0.5);
        this.load.setPreloadSprite(this.preloadBar);
        // display game title text above the loading bar
        this.titleText = this.add.image(this.world.centerX, this.world.centerY - 220, 'titleimage');
        this.titleText.anchor.setTo(0.5, 0.5);

        // load custom font
        this.load.bitmapFont('eightbitwonder', 'fonts/font.png', 'fonts/font.fnt');

        // load remaining game assets, images, audio and spritesheets
        this.load.image('startGameScreen', 'images/startmenu.png');
        this.load.image('alienPlanet', 'images/planet.png');
        this.load.image('guns', 'images/guns.png');
        this.load.image('rocket', 'images/rocket.png');
        this.load.image('moonrock', 'images/moonrock.png');
        this.load.image('gameButton', 'images/button_main.png');
        this.load.image('replayButton', 'images/button_replay.png');
        this.load.image('bullet', 'images/plasma.png');
        this.load.image('fire1', 'images/fire1.png');
        this.load.image('fire2', 'images/fire2.png');
        this.load.image('fire3', 'images/fire3.png');
        this.load.image('smoke', 'images/smoke-puff.png');
        this.load.image('star', 'images/star.png');
        this.load.spritesheet('kaboom', 'images/explode.png', 128, 128);

        this.load.audio('explosionAudio', './audio/explosion.wav');
        this.load.audio('collectedAudio', './audio/collected.wav');
        // set ready as true as all assets shuld now be loaded
        this.ready = true;
    },

    create: function () {
        console.log("Preloader create...");
        // dont crop loading bar
        this.preloadBar.cropEnabled = false;
    },

    update: function () {
        // if assets are loaded move to the start menu screen
        if (this.ready) {
            this.state.start('StartMenu');
        }
    }
};