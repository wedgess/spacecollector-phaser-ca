SpaceCollector.GameOver = function (game) {
    var endScore = 0;
}


SpaceCollector.GameOver.prototype = {
    init: function (score) {
        console.log("You scored: " + score);
        // get the score passed in from GameOver.js
        this.endScore = score;

    },
    preload: function () {
        // load the tilemap JSON
        this.game.load.tilemap('space', 'tilemap/GameOverMap.json', null, Phaser.Tilemap.TILED_JSON);
        // load image used for tilemap
        this.game.load.image('tile', 'tilemap/tile_map_new.png');
    },
    create: function () {
        // add the tilemap to the game
        var map = this.game.add.tilemap('space');
        // add the tile maps image to the tilemap
        map.addTilesetImage('tile_map_new', 'tile');

        // draw tilemaps GB layer as GB
        var background = map.createLayer('bg');
        background.resizeWorld();
        
        // show game over text using custom bitmap text
        var gameOverTitle = this.add.bitmapText((this.world.centerX/2)+150, this.world.centerY-320, 'eightbitwonder', 'Game Over', 92);
        gameOverTitle.anchor.setTo(0.5, 0.5);
        // display image of moonrock next to users score
        var moonrock = this.add.image((this.world.centerX/2)+60, this.world.centerY-20,'moonrock');
        moonrock.scale.setTo(1.3);
        // display text using custom bitmap font
        var yourScoreTitle = this.add.bitmapText((this.world.centerX/2)+145, this.world.centerY-110, 'eightbitwonder', 'You Scored', 48);
        yourScoreTitle.anchor.setTo(0.5, 0.5);
        // add players score text
        this.add.bitmapText((this.world.centerX/2)+160, this.world.centerY-30, 'eightbitwonder', this.endScore, 84);
        
        // add replay game button
        var gameButton = this.game.add.button(this.world.centerX, this.world.centerY +320, 'replayButton', this.replayGame, this);
        gameButton.anchor.setTo(0.5, 0.5);
        gameButton.scale.setTo(0.9, 0.9);
    },
    replayGame: function () {
        // callback function to replay the game
        this.game.state.start("Game");
    }
};