SpaceCollector.Game = function (game) {
    var rocket;
    var scoreLabel;
    var levelLabel;
};
// holds the players score
var playerScore = 0;
// holds players current level
var playerLevel = 1;

// angle of the rocket as its moving
var angle = 0;
// speed which the rocket moves
var speed = 1.2;
// direction of the rocket - false as we initially set it to true when buttons clicked
var clockwise = false;
// the circle which the rocket goes around
var circleDiameter = 540;
// line width(stroke) of above circle
var lineSize = 4;
// used in update so that the rocket doesn't move until button is pressed (game started)
var gameHasStarted = false;
// distance for where game content starts on Y axis
var yOffset = 120;
// the outline circle radius
var radius = (circleDiameter - (lineSize * 2)) / 2 + lineSize;
// holds the rocks with are initially displayed on screen
var rockArray = [];
// holds the rocks from the above array with have been collected
var collectedRockArray = [];
// whether or not game over has been called, without this it can be called multiple times
var isGameOverCalled = false;

var bullet;
var bullets;
// the time between when bullets fire (each bullet has a group of 3)
var bulletTime = 1200;
// the rocket engines emission object
var emitter;
// the audio for the explosion
var explosion_audio;
// audio for collecting rock
var collected_audio;
// the main game button
var gameButton;

SpaceCollector.Game.prototype = {
    // ------------------------------------------------- Phaser functions -----------------------------------------------//
    create: function () {
        console.log("In game create()");
        console.log("rockArray length: " + rockArray.length);
        // set player score as 0 needed for when we restart game from GameOver.js
        playerScore = 0;
        // set up the game background
        this.initGameBG();
        // set up player score and level labels
        this.initGameLabels();
        // center the planet in x and y adding offset to y
        var planet = this.game.add.sprite(this.world.centerX, this.world.centerY - yOffset, 'alienPlanet');

        // helps with centering the planet
        planet.anchor.setTo(0.5);
        // scale the planet to be slightly larger
        planet.scale.setTo(1.2);

        // initalize the bullets and their properties
        this.initBullets();

        // center the guns in x and y adding offset to y (added after planet so it sits on top)
        var guns = this.game.add.sprite(this.world.centerX, this.world.centerY - yOffset, 'guns');

        // helps with centering the guns
        guns.anchor.setTo(0.5);
        // scale the guns image
        guns.scale.setTo(1.6);

        // draw the game circle
        this.initGameCircle();

        // when the user clicks the replay button in game over screen, the rocks array items
        // are added to the previous array. This means the rocket keeps going after all rocks
        // are collected because the rock array size has doubled etc...
        // i have tried resetting alpha of rock array but they are still hidden so resetting the array
        // is a workaround for now!
        if (rockArray.length != 0) {
            rockArray = [];
        }
        // set up the moonrock array, must be called after the above if statement or rock array will be empty
        this.initMoonRocks();
        // set up the rockets emitter
        this.initRocketEmitter();

        // set up the rocket
        this.initRocket();

        // set up the game button - which changes the rockets direction
        this.initGameButton();

        // set up the explosion pool for when a bullet hits the rocket
        this.initExplosions();
    },
    // update is called multiple times in a second, it is a phaser function for a state
    update: function () {
        //console.log("In game update()");
        // if the game has started
        if (gameHasStarted) {
            // decrease/increase angle depending on rocket direction
            if (clockwise) {
                angle += speed;
            } else {
                angle -= speed;
            }

            // if the angle is greater or equal to 360 subtract 360 from it
            if (angle >= 360) {
                angle = 360 - angle;
            }

            // if the rock array is not empty it means more there are still moonrocks uncollected
            if (rockArray.length != 0) {
                // move rocket on the orbit of the circle
                this.moveRocketOnCircle(angle);

                // collison did not work here nor did having a callback function to check if they overlapped
                // the workaround was to check if each of the remaining moonrocks have been overlapped, within
                // the rockArray
                for (var i = 0; i < (rockArray.length); i += 1) {
                    if (this.game.physics.arcade.overlap(this.rocket, rockArray[i])) {
                        // they are overlapping collect the moon rock and add it to the collected rock array
                        this.collectedRock(rockArray[i]);
                    }
                }
                // fire the bullet
                this.fireBullet();
                // add callback for if the bullet collides witht the rocket
                this.game.physics.arcade.collide(this.rocket, bullets, null, this.gameOverFinish, this);
                // update the rocket emitter
                this.updateRocketEmitter();

            } else {
                // rock array is empty don't run code in update anymore
                gameHasStarted = false;
            }
        } else {
            // rock array length is - and gam over not called
            if (rockArray.length == 0 && !isGameOverCalled) {
                // hide the emitter on new level
                emitter.alpha = 0;
                // tween out the rocket
                this.game.add.tween(this.rocket).to({
                    alpha: 0
                }, 300, Phaser.Easing.Circular.InOut, true);
                isGameOverCalled = true;
                console.log("Resetting game level");
                // kill all the bullets so they can;t hit rocket
                bullets.callAll('kill');
                // add a 400 MS delay before resetting the level
                this.game.time.events.add(Phaser.Timer.SECOND * 0.4, this.resetLevel, this);
            }
        }

    },
    // ----------------------------------------- Initialize the game elements ------------------------------------------ //
    // initialize the games BG
    initGameBG: function () {
        // make bitmap cover full screen
        var bgBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
        // add a linear gradient
        var gradient = bgBitmap.context.createLinearGradient(0, 600, 0, 800);
        // starts as black
        gradient.addColorStop(0, "#000000");
        // ends at a dark blue
        gradient.addColorStop(1, "#0F1027");
        bgBitmap.context.fillStyle = gradient;
        bgBitmap.context.fillRect(0, 0, this.game.width, this.game.height);
        var screen = this.game.add.sprite(0, 0, bgBitmap);
    },
    // initialize the score label and the current level label
    initGameLabels: function () {
        var levelText = "Level: 1";
        //score text   
        var scoreText = "Score: 0";
        // set the text style for both labels
        var style = {
            font: "26px Arial",
            fill: "#fff",
            align: "center"
        };
        this.scoreLabel = this.add.text(20, 20, scoreText, style);
        this.levelLabel = this.add.text(this.world.width - 135, 20, levelText, style);
    },
    // initialize the bullets
    initBullets: function () {
        // set the bullets group   
        bullets = this.game.add.group();
        // enabling physics on the bullets
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.physics.arcade.enable(bullets);
        //  add multiple bullets to group  
        bullets.createMultiple(5, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 0.5);
    },
    // initialize the game circle
    initGameCircle: function () {
        // draw a circle at position ...
        var circle = this.add.graphics(this.world.centerX, this.world.centerY - yOffset);
        // add a stroke to the circle, no fill
        circle.lineStyle(lineSize, 0xffffff);
        circle.drawCircle(0, 0, circleDiameter);
        // set the alpha of the circle
        circle.alpha = 0.16;
    },
    // initialize the moon rocks
    initMoonRocks: function () {
        // the moon rock distance from eachother
        var rockDistance = 0.238;
        // we add this to i so that the rocks levae a gap at the top of the circle for the rocket
        var multiplier = 21;
        // starting at 28 allows to create a space for the rocket at the start position
        for (var i = 0; i < 25; i++) {
            // moonrocks x and y coordinates
            var x = Math.round(this.game.width / 2 + radius * Math.cos((i + multiplier) * rockDistance) - lineSize / 2),
                y = Math.round((this.game.height - (yOffset * 2)) / 2 + radius * Math.sin((i + multiplier) * rockDistance) - lineSize / 2);
            //console.log("rock " + i + " X is: " + x);
            //console.log("rock " + i + " Y is: " + y);
            // add moonrock
            var moonrock = this.add.sprite(x, y, 'moonrock');
            moonrock.alpha = 0;
            // set scale of moonrock
            moonrock.scale.setTo(0.5);
            moonrock.anchor.setTo(0.5);
            // enabling physics
            this.physics.arcade.enable(moonrock);
            moonrock.enableBody = true;
            moonrock.physicsBodyType = Phaser.Physics.ARCADE;

            // add a fade in tween for the moonrocks
            this.game.add.tween(moonrock).to({
                alpha: 1
            }, 200 + (i * 60), Phaser.Easing.Linear.None, true);
            // add moonrock to array
            rockArray.push(moonrock);
        }
        // add audio for the explosion // TODO: Not working yet
        collected_audio = this.add.audio('collectedAudio');
    },
    // initialize the rocket emitter
    initRocketEmitter: function () {
        // start emitter where the rocket is positioned
        emitter = this.add.emitter(this.world.centerX - 42, ((this.world.height - (yOffset * 2)) / 2) - radius, 400);
        // add particles as an array from what was loaded in Preloader state
        emitter.makeParticles(['fire1', 'fire2', 'fire3', 'smoke']);
        emitter.gravity = 200;
        emitter.setAlpha(1, 0, 1000);
        emitter.setScale(0.2, 0, 0.2, 0, 2000);
        emitter.start(false, 3000, 5);
        emitter.alpha = 0;
    },
    // initilaize the games rocket ship
    initRocket: function () {
        // rocket sprite
        this.rocket = this.add.sprite(this.world.centerX, ((this.world.height - (yOffset * 2)) / 2) - radius, 'rocket');
        // set initial alpha as 0 as we use a tween to fade in
        this.rocket.alpha = 0;
        this.rocket.anchor.setTo(0.5);
        // enable physics on rocket
        this.physics.arcade.enable(this.rocket);
        this.rocket.enableBody = true;
        this.rocket.physicsBodyType = Phaser.Physics.ARCADE;
        // add the tween so rocket animates in in game start
        this.game.add.tween(this.rocket).to({
            alpha: 1
        }, 600, Phaser.Easing.Linear.None, true);
    },
    // initialize the explosions for when bullet hits rocket
    initExplosions: function () {
        // Setup An explosion pool
        explosions = this.add.group();
        // this is the only way of adding without making visible
        explosions.createMultiple(1, 'kaboom');
        explosions.forEach(function (explosion) {
            explosion.anchor.setTo = 0.5;
            explosion.animations.add('kaboom');
        }, this);
        // add audio for the explosion // TODO: Not working yet
        explosion_audio = this.add.audio('explosionAudio');
    },
    initGameButton: function () {
        // the main button in the game to change rocket position
        gameButton = this.game.add.sprite(this.world.centerX, this.world.centerY + (yOffset * 2) + 60 + (yOffset / 2), 'gameButton');
        gameButton.frame = 0;
        //gameButton = this.add.button(this.world.centerX, this.world.centerY + (yOffset * 2) + 60 + (yOffset / 2), 'gameButton', this.changeRocketDirection, this, 1, 0, 0);
        gameButton.anchor.setTo(0.5, 0.5);
        gameButton.scale.setTo(0.9, 0.9);
        // enable touch
        gameButton.inputEnabled = true;
        // bind event handler to touch down
        gameButton.events.onInputDown.add(this.changeRocketDirection, this);
        //gameButton.maxPointers = 1;
    },
    //------------------------------------------------- Main Game function ----------------------------------------------//
    // changes the direction of the rocket
    changeRocketDirection: function () {
        // This is needed as we only want to listen to mouse down as an input event
        // has mouse up and down event so this is called twice on XDK emulator
        if (this.game.input.activePointer.leftButton.isDown) {
            // if game has sctarted then change rocket direction
            if (gameHasStarted) {
                // set the clockwise boolean to opposite of what it was
                clockwise = !clockwise;
                console.log("Changed rocket direction to clockwise: " + clockwise);
                //this.rocket.rotation = clockwise ? 180 : 0;
                //console.log(this.rocket);
            } else {
                gameHasStarted = true;
                // reshow the emitter
                emitter.alpha = 1;
            }
        }
    },
    // called when rocket collects a moon rock
    collectedRock: function (rock) {
        collected_audio.play();
        // increase the players score
        playerScore++;
        // set the score on the label
        this.scoreLabel.text = "Score: " + playerScore;
        //rock.kill();
        // get index of the rock in the rockArray
        var index = rockArray.indexOf(rock);
        // fade rock out when collected
        this.game.add.tween(rock).to({
            alpha: 0
        }, 200, Phaser.Easing.Linear.None, true);
        //rock.kill();
        // if index of the rock is a valid position
        if (index > -1) {
            // remove item from array after collision to make animation smoother
            rockArray.splice(index, 1);
        }
        // save destroying and reloading rocks again, save them for next level
        collectedRockArray.push(rock);

    },
    // called when a bullet should be fired
    fireBullet: function () {
        // make sure the bullet time has passed before firing
        if (this.game.time.now > bulletTime) {
            // get bullet
            bullet = bullets.getFirstExists(false);
            // if bullet is not empty
            if (bullet) {
                // set its position to the center of the planet, under the cannons
                bullet.reset(this.world.centerX, this.world.centerY - yOffset);
                // bullet lives for 2 seconds
                bullet.lifespan = 2000;
                // set its rotation as the rockets, not important as the bullets are circle
                bullet.rotation = this.rocket.rotation;
                // set bullet speed, this increases as players level increases
                var bulletSpeed = 290 + (playerLevel * 10);
                console.log("BulletSpeed: " + bulletSpeed);
                // helps the bullets follow the ship when the rocket changes direction
                if (clockwise) {
                    console.log("Clockwise");
                    this.game.physics.arcade.velocityFromRotation((this.rocket.rotation + this.game.rnd.frac()), bulletSpeed, bullet.body.velocity);
                } else {
                    console.log("AntiClockwise");
                    this.game.physics.arcade.velocityFromRotation((this.rocket.rotation - this.game.rnd.frac()), bulletSpeed, bullet.body.velocity);
                }
                // update bullet time
                bulletTime = this.game.time.now + 400;
            }
        }
    },
    // moves the rocket along the circle
    moveRocketOnCircle: function (deg, sprite) {
        // convert the degrees to radians and get new x and y values
        var theta = Phaser.Math.degToRad(deg)
        var newX = Math.sin(theta) * radius;
        var newY = Math.cos(theta) * radius;

        this.rocket.x = this.game.width / 2 - newX;
        this.rocket.y = (this.game.height - (yOffset * 2)) / 2 - newY
            // rotate the rocket, this also handles the direction of the rocket depending on button clicks
        this.rocket.angle = (clockwise ? 180 : 0) - deg;

    },
    // updates the rockets emitter so it follows the rocket
    updateRocketEmitter: function () {
        // get rockets x and y
        var px = this.rocket.body.velocity.x;
        var py = this.rocket.body.velocity.y;

        // make px and py positiove incase negative
        px *= -1;
        py *= -1;

        emitter.minParticleSpeed.set(px - 100, py - 100);
        emitter.maxParticleSpeed.set(px, py);

        emitter.emitX = this.rocket.x;
        emitter.emitY = this.rocket.y;
    },
    // --------------------------------------------------- Gameover --- reset etc ---------------------------------------//
    gameOverFinish: function (rocket, bullet) {
        explosion_audio.play();
        // disable game button presses
        gameButton.events.onInputDown.remove(this.changeRocketDirection, this);
        // kill rocket and bullet sprite
        rocket.kill();
        bullet.kill();
        // jide emitter
        emitter.alpha = 0;
        gameHasStarted = false;
        isGameOverCalled = false;
        console.log("Bullet hit ship");
        //  And create an explosion :)
        // play explosion
        var explosion = explosions.getFirstExists(false);
        explosion.reset(this.rocket.body.x, this.rocket.body.y);
        explosion.play('kaboom', 40, false, true);
        // reset game labels
        this.scoreLabel.text = "Score: 0";
        this.levelLabel.text = "Level: 1";
        // set a temporary variable to store the score, as we reset it below but pass it to the GameOver.js init function
        var tempScore = playerScore;
        playerLevel = 0;
        playerScore = 0;
        // reset level and show game over screen after 1.5 second, allow for explosion animation & audio to finish, adding a animationComplete listener didn;t work here
        this.game.time.events.add(Phaser.Timer.SECOND * 1.5, function () {
            explosion_audio.stop();
            this.resetLevel();
            this.state.start('GameOver', true, false, tempScore);
        }, this);

    },
    // reset the games level if all rocks are collected and player didn;t die
    resetLevel: function () {
        //this.rocket.revive();
        console.log("collected rock array size: " + collectedRockArray.length);

        // reset rocket position emotter and tween in the rocket after 200 MS
        this.game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
            this.rocket.aplha = 0;
            // reset rocket
            this.rocket.x = this.world.centerX;
            this.rocket.y = ((this.world.height - (yOffset * 2)) / 2) - radius;
            this.updateRocketEmitter();
            this.game.add.tween(this.rocket).to({
                alpha: 1
            }, 600, Phaser.Easing.Circular.InOut, true);
        }, this);
        angle = 0;
        clockwise = false;
        playerLevel++;
        isGameOverCalled = false;
        this.levelLabel.text = "Level: " + playerLevel;
        this.rocket.angle = 0;
        this.rocket.body.velocity = 0;
        // re-add all collected rocks to the main rock array and fade them in
        for (var i = 0; i < collectedRockArray.length; i++) {
            var rock = collectedRockArray[i];
            // add animation for re-showing the rocks
            this.game.add.tween(rock).to({
                alpha: 1
            }, 200 + (i * 60), Phaser.Easing.Linear.None, true);
            rockArray.push(rock);
        }
        // reset collected rocks array
        collectedRockArray = [];
    }
};