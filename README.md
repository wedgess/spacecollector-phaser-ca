# Casual Game with Phaser #
=========================================================

### Brief ###
Create a prototype for a casual game with the Phaser Game Framework. This could be an expanded version of the  sample we were working in class.
* loading of graphics using preload state
* Tilemap for Game environment
 * tweens, animation of characters using a spritesheet
* sound
* arcade physics


**The game is to be run on mobile.** 

### Code ###

Your code is the major contribution to this CA. It needs to be implemented in Javascript classes. The code should be well commented. Your program code should be readable.  You will need to demonstrate that this is your own work. "Found" or "shared" code is not an acceptable submission and will lead to a fail.


# My Finished Game #
=================================

![Alt text](http://i.imgur.com/XvKpgDd.png "Home Screen")

![Alt text](http://i.imgur.com/PVnNqJh.png "Game Screen")

![Alt text](http://i.imgur.com/vEYPuQ1.png "Game OverScreen")